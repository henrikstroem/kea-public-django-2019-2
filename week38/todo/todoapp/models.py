from django.db import models


class TodoItem(models.Model):
    title = models.CharField(max_length=200)
    status = models.BooleanField()
    description = models.TextField(blank=True)