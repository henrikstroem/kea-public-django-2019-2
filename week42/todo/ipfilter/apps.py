from django.apps import AppConfig


class IpfilterConfig(AppConfig):
    name = 'ipfilter'
