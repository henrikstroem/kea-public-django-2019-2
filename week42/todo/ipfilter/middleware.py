from django.utils.deprecation import MiddlewareMixin
from django.core.exceptions import PermissionDenied


class IPFilterMiddleware(MiddlewareMixin):

    def __init__(self, get_response):
        self.get_response = get_response
        print("*** Middleware init")

    def __call__(self, request):
        print("*** Middleware call request")
        # Code to be executed for each request before
        # the view (and later middleware) are called.
        self.process_request(request)

        response = self.get_response(request)

        print("*** Middleware call response")
        # Code to be executed for each request/response after
        # the view is called.
        self.process_response(request, response)

        return response

    def process_request(self, request):
        print("*** Middleware process request")
        allowed_ip_addresses = ['127.0.0.1']
        ip = request.META.get('REMOTE_ADDR')
        print(f"*** IP address {ip} detected")
        if not ip in allowed_ip_addresses:
            raise PermissionDenied
        return None

    def process_response(self, request, response):
        print("*** Process response")
        response["X-IP-FILTER"] = "IP FILTER BY KEA"
        return response
        