from functools import reduce
from operator import mul


def add(*args):
    return sum(args)

def mult(*args):
    if args:
        if not len(args) == 1:
            return reduce(mul, args)
        else:
            raise ValueError("I can't multiply 1 number, dummy!")
    else:
        return 0
