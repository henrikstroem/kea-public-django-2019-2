class Hatchback:

    def __init__(self, **kwargs):
        for k in kwargs:
            self.__setattr__(k, kwargs[k])


if __name__ == '__main__':
    car = Hatchback(top_speed=168)
    print(car.top_speed)