import unittest

import mathlib

class AddTests(unittest.TestCase):

    def test_positive(self):
        self.assertEqual(mathlib.add(1, 2), 3)
        self.assertEqual(mathlib.add(1, 2, 3, 4), 10)
        self.assertNotEqual(mathlib.add(2, 3), 7)
        self.assertEqual(mathlib.add(1_000_000, 1), 1_000_001)

    def test_negative(self):
        self.assertEqual(mathlib.add(-1, -5), -6)


class MultTests(unittest.TestCase):

    def test_positive(self):
        self.assertEqual(mathlib.mult(2, 3), 6)
        self.assertEqual(mathlib.mult(1, 2, 3, 4), 24)
        
    def test_zero_cases(self):
        self.assertEqual(mathlib.mult(3, 4, 6, 0), 0)

    def test_bad_data(self):
        self.assertEqual(mathlib.mult(), 0)
        self.assertRaises(ValueError, mathlib.mult, 1)